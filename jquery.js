// Fonction de roll pour l'attaquant
function rollForPlayer1(){
    let roll = Math.floor(Math.random()*100)+1;
    $("#resultat_1").empty();
    $("#resultat_1").append(roll);
    $("#de1").attr("src", "img/roll_ok.png");
  
      if((roll >= 1) && (roll <= 5)) {
        $("#phrase1").append("Echec Critique. Vous aurez un malus de 5 au prochain dé.");
      }
      else if((roll >= 95) && (roll <= 100)) {
        $("#phrase1").append("Réussite Critique. Vous gagnez une attaque surprise.");    }
      else {
        $("#phrase1").append("Votre lancer doit être comparé à celui du Défenseur.");
      }
  };
  
  // Fonction de roll pour le défenseur
  function rollForPlayer2(){
    let roll = Math.floor(Math.random()*100)+1;
    $("#resultat_2").empty();
    $("#resultat_2").append(roll);
    $("#de2").attr("src", "img/roll_ok.png");
  
    if((roll >= 1) && (roll <= 5)) {
      $("#phrase2").append("Echec Critique. Vous aurez un malus de 10 au prochain dé et une jolie blessure.");
    }
    else if((roll >= 95) && (roll <= 100)) {
      $("#phrase2").append("Réussite Critique. Vous avez un bonus de 5 au prochain dé.");
    }
    else {
      $("#phrase2").append("Votre lancer doit être comparé à celui de l'attaquant.");
    }
  };
  
  
  // Fonction qui permet d'effacer les lancers et résultats
  function refaire() {
    $("#de1").attr("src", "img/roll.png");
    $("#de2").attr("src", "img/roll.png");
    $("#resultat_1").empty();
    $("#resultat_2").empty();
    $("#phrase1").empty();
    $("#phrase2").empty();
  };