// On crée les variables dont on a besoin
var roll1 = "";
var roll2 = "";
var img1 = document.getElementById("de1");
var img2 = document.getElementById("de2");


// Fonction de roll pour l'attaquant
function rollForPlayer1(){

  roll1 = Math.floor(Math.random()*100)+1;
  // document.getElementById("resultat_1").innerHTML = roll1;  <= Suppression de l'affichage des rolls en dessous des dés.
  img1.src = "img/roll_ok.png";

  return roll1;
};

// Fonction de roll pour le défenseur
function rollForPlayer2(){

  if (roll1 == "") 
    {
      alert('Merci de cliquer sur le dé de l\'attaquant.');
      disabled(afficheResultat());
    }
  else
    {
      roll2 = Math.floor(Math.random()*100)+1;
      // document.getElementById("resultat_2").innerHTML = roll2; <= Suppression de l'affichage des rolls en dessous des dés.
      img2.src = "img/roll_ok.png";

      return roll2;
    }
};


function afficheResultat(){

  // roll1 =4;
  // roll2 =2; 

  var newResultats;
  var newAttaquant = document.createElement("p");
  var resultatAttaquant = document.createTextNode("Attaquant : " + roll1);
  newAttaquant.appendChild(resultatAttaquant);
  var newDefenseur = document.createElement("p");
  var resultatDefenseur = document.createTextNode("Défenseur : " + roll2);
  newDefenseur.appendChild(resultatDefenseur);
  var newResultat = document.createElement("p");

  // Voir pour rajouter les bonus / malus de dé avec la fonction last()  du style   roll1.last(); 

  switch (true) {
    //Cas du Roll 1 Supérieur
    case roll1 > roll2 : 
      {
        if ((roll1 >= 1) && (roll1 <= 5) )  
          {
            resultat = document.createTextNode("L'attaquant réussi péniblement son attaque.");
            newResultat.appendChild(resultat);
            newResultats = document.createElement('div');
            newResultats.classList.add('score');
            newResultats.appendChild(newAttaquant);
            newResultats.appendChild(newDefenseur);
            newResultats.appendChild(newResultat);
            document.getElementById('resultats').appendChild(newResultats);
          }
        else if ((roll1 >= 95) && (roll1 <= 100)) 
          {
            resultat = document.createTextNode("L'attaquant réussi une attaque critique. Il gagne une attaque surprise.");
            newResultat.appendChild(resultat);
            newResultats = document.createElement('div');
            newResultats.classList.add('score');
            newResultats.appendChild(newAttaquant);
            newResultats.appendChild(newDefenseur);
            newResultats.appendChild(newResultat);
            document.getElementById('resultats').appendChild(newResultats);
          }
        else 
          {
            resultat = document.createTextNode("L'attaquant réussi son attaque.");
            newResultat.appendChild(resultat);
            newResultats = document.createElement('div');
            newResultats.classList.add('score');
            newResultats.appendChild(newAttaquant);
            newResultats.appendChild(newDefenseur);
            newResultats.appendChild(newResultat);
            document.getElementById('resultats').appendChild(newResultats);
          }
      };
      break;
    // En cas d'égalité
    case roll1 == roll2 :
          {
            resultat = document.createTextNode("La touche n'est pas comptabilisée.");
            newResultat.appendChild(resultat);
            newResultats = document.createElement('div');
            newResultats.classList.add('score');
            newResultats.appendChild(newAttaquant);
            newResultats.appendChild(newDefenseur);
            newResultats.appendChild(newResultat);
            document.getElementById('resultats').appendChild(newResultats);
          };
      break;
    // Cas du roll 2 supérieur 
    case roll1 < roll2 :
      {
        if ((roll2 >= 1) && (roll2 <= 5) ) 
          {
            resultat = document.createTextNode("La défense réussi péniblement.");
            newResultat.appendChild(resultat);
            newResultats = document.createElement('div');
            newResultats.classList.add('score');
            newResultats.appendChild(newAttaquant);
            newResultats.appendChild(newDefenseur);
            newResultats.appendChild(newResultat);
            document.getElementById('resultats').appendChild(newResultats); 
          }
          else if ((roll2 >= 95) && (roll2 <= 100)) 
          {
            resultat = document.createTextNode("Le défenseur réussi avec brio.");
            newResultat.appendChild(resultat);
            newResultats = document.createElement('div');
            newResultats.classList.add('score');
            newResultats.appendChild(newAttaquant);
            newResultats.appendChild(newDefenseur);
            newResultats.appendChild(newResultat);
            document.getElementById('resultats').appendChild(newResultats);
          }
        else 
          {
            resultat = document.createTextNode("La défense réussi.");
            newResultat.appendChild(resultat);
            newResultats = document.createElement('div');
            newResultats.classList.add('score');
            newResultats.appendChild(newAttaquant);
            newResultats.appendChild(newDefenseur);
            newResultats.appendChild(newResultat);
            document.getElementById('resultats').appendChild(newResultats);
          }
      };
      break;
  };

  //barre de scroll avec position initiale en bas, pour voir les derniers résultats affichés
  barreScroll = document.getElementById('resultats');
  barreScroll.scrollTop = barreScroll.scrollHeight;

  //remise à l'état normal des images des dés.
  img1.src = "img/roll.png";
  img2.src = "img/roll.png";

  //remise à Zéro des dés
  roll1 = "";
  roll2 = "";
};



// Fonction qui permet de remettre les images de dés à leur état de départ.
// function refaire() {

//   img1.src = "img/roll.png";
//   img2.src = "img/roll.png";
// };

// fonction qui permet de vider la div des résultats pour remettre le salon à 0
function effaceResultats() {
  var parent_node = document.getElementById('resultats'); 
  parent_node.innerHTML =""; 
};





