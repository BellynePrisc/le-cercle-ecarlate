// On crée les variables dont on a besoin
var roll1 = "";
var roll2 = "";
var img1 = document.getElementById("de1");
var img2 = document.getElementById("de2");


// Fonction de roll pour l'attaquant
function rollForPlayer1(){
  roll1 = Math.floor(Math.random()*100)+1;
  document.getElementById("resultat_1").innerHTML = roll1;
  img1.src = "img/roll_ok.png";

    if((roll1 >= 1) && (roll1 <= 5)) {
      document.getElementById("phrase1").innerHTML = "Echec Critique. Vous aurez un malus de 5 au prochain dé.";
    }
    else if((roll1 >= 95) && (roll1 <= 100)) {
      document.getElementById("phrase1").innerHTML = "Réussite Critique. Vous gagnez une attaque surprise.";
    }
    else {
      document.getElementById("phrase1").innerHTML = "Votre lancer doit être comparé à celui du Défenseur.";
    }

};

// Fonction de roll pour le défenseur
function rollForPlayer2(){
  roll2 = Math.floor(Math.random()*100)+1;
  document.getElementById("resultat_2").innerHTML = roll2;
  img2.src = "img/roll_ok.png";

  if((roll2 >= 1) && (roll2 <= 5)) {
    document.getElementById("phrase2").innerHTML = "Echec Critique. Vous aurez un malus de 10 au prochain dé et une jolie blessure.";
  }
  else if((roll2 >= 95) && (roll2 <= 100)) {
    document.getElementById("phrase2").innerHTML = "Réussite Critique. Vous avez un bonus de 5 au prochain dé.";
  }
  else {
    document.getElementById("phrase2").innerHTML = "Votre lancer doit être comparé à celui de l'Attaquant.";
  }
};


// Fonction qui permet d'effacer les lancers et résultats
function refaire() {
  img1.src = "img/roll.png";
  img2.src = "img/roll.png";
  document.getElementById("resultat_1").innerHTML = "";
  document.getElementById("resultat_2").innerHTML = "";
  document.getElementById("phrase1").innerHTML = "";
  document.getElementById("phrase2").innerHTML = "";
};





